#!/usr/bin/env ruby
# encoding: UTF-8

# Picture Grabber 

require 'net/http'

url_list = Array.new
url_list << ARGV[0] if !ARGV[0].nil?
File.readlines("autodownload.lst").each do |s| 
  url_list << s.chomp unless s.match?(/^\s+$/) || s == ""
end if File.exist? "autodownload.lst"

if url_list.size == 0
  puts "Provide the thread url as command line argument,\nor in autodownload.lst. "
  exit
end

url_list.uniq!


def connection_setup(host, port)
  $net = Net::HTTP.new host, port 
  $net.use_ssl = port == 443 ? true : false
  $net.open_timeout = 20
  $net.read_timeout = 20
end

$links = Array.new
$links404 = Array.new
$detected404 = false

def page_check(file, url)
  File.readlines(file).each do |l|
    if l.match?(/<title>.*404 Not Found.*<\/title>/i)
      puts "404 Not Found."
      $links404 << url
      $detected404 = true
      move_finished($thread_name)
      throw :skip
    elsif l.match?(/<title>.*400 Bad Request.*<\/title>/i)
      puts "400 Bad Request."
      throw :skip
    elsif l.match?(/<title>.*421 Misdirected Request.*<\/title>/i)
      puts "421 Bad Request."
      throw :skip
    end
  end
end

def grab(url) 
  thread_url = url.match?(/^https?:\/\//) ? url : "https://" + url

  host = thread_url.sub(/https?:\/\//, "")[/^[a-z0-9\.\-]+\.[a-z]+/]
  $thread_name = thread_url[/[a-z0-9_\-\.]+$/i]
  $thread_name = host unless $thread_name

  Dir.mkdir "./#{$thread_name}" unless Dir.exist? "./#{$thread_name}"

  !!(thread_url =~ /^https:\/\//) ? port = 443 : port = 80

  connection_setup(host, port)

  File.new "./#{$thread_name}/tmp.txt", "w"
  tmp = File.open "./#{$thread_name}/tmp.txt", "r+"

  print "Getting " + thread_url + " page ... "
  tmp << $net.get(thread_url).body
  tmp.close

  page_check(tmp, thread_url)
  print "Done.\n"
  $links << thread_url

  main_buffer = File.read("./#{$thread_name}/tmp.txt").scan /href="?[\wа-я\.\-_:\/ ]+"?/i

  File.new "#{$thread_name}/file_list.html", "w"
  file_list = File.open "./#{$thread_name}/file_list.html", "r+" 
  file_list << Time.now.strftime("%Y-%m-%d %H:%M:%S") + "<br>\n"
  file_list << "\n<a href=\"#{thread_url}\">#{thread_url}</a><br><br>\n"


  last_path = "" 
  last_host = ""
  main_buffer.each do |s| 
    working_str = s[/href="?(https?:)?(\/\/)?([\w\-\.]+\.[a-z]+)?\/[\/\w\-]+\/[\wа-я\-\.]+\.(jpe?g|png|webp|jfif|flif|fuif|jxl)/i]

    if working_str
      file_path = working_str.sub(/href="?(\/\/)?/, '')

      if file_path.match?(/^\//)
        file_path = host + file_path
      else
        host = file_path[/^[\w\-\.]+\.[a-z]+/]
      end

      unless file_path == last_path
        file_name = file_path[/[\wа-я\-\.]+\.(jpe?g|png|webp)$/i]

        if file_path.match?(/^https?:\/\//)
          file_url = file_path
        else
          file_url = "http" + (port == 443 ? "s://" : "://") + file_path 
        end

        file_list << "<a href=\"\.\/" + file_name + "\">" + file_name + "<\/a><br>\n"

        unless host == last_host
          connection_setup(host, port)
        end

        unless File.exist? "./#{$thread_name}/#{file_name}"
          File.new "./#{$thread_name}/#{file_name}", "w" 
          img_file = File.open "./#{$thread_name}/#{file_name}", "w"
          print "Downloading " + file_url + " ... "
          begin
            img_file << $net.get(file_url).body
            print "Done\n" 
          rescue
            print "Timeout.\n"
            img_file.close
            if File.exist? "./#{$thread_name}/#{file_name}"
              File.delete "./#{$thread_name}/#{file_name}" 
              print "Deleted.\n"
            end
            attempts += 1
            if attempts <= 5
              print "Retrying..."
              retry
            else
              print "Failed."
            end
          end
        end
      end
      last_path = file_path
      last_host = host
    end
  end
end

def move_finished(src)
  dest = "./finished/#{src}"
  if Dir.exist? dest
    dest = "#{dest}-#{'%04x' % (rand 0xffff)}"
    Dir.mkdir dest
  else
    Dir.mkdir dest
  end
  
  Dir.each_child("./#{src}") do |f|
    next if File.exist? "#{dest}/#{f}"
    File.rename "./#{src}/#{f}", "#{dest}/#{f}"
  end
  Dir.rmdir src
end


url_list.each do |s| 
  catch :skip do
    grab(s)
  end
  puts
end


if $detected404
  tmp = File.open "./autodownload.lst", "w"
  $links.each { |l| tmp << l + "\n" }
  tmp.close

  File.foreach("./finished.lst") { |l| $links404 << l.chomp }
  tmp = File.open "./finished.lst", "w"
  $links404.unshift(Time.now.strftime("%Y-%m-%d %H:%M:%S"))
  $links404.each { |l| tmp << l + "\n" }
  tmp.close
end

