# Picture Grabber

Picture Grabber is a script for downloading images from imageboard threads.  
*Grabber.rb* creates a new directory for each thread, so to keep things tidy, you might want to put it into a separate directory.  

### Usage
Provide *grabber.rb* a link to a thread as a cli argument. For example:  
```shell
$ grabber.rb https://boards.4channel.org/p/thread/3910581/rpt-recent-photos-thread
```

You can also create *autodownload.lst* and populate it with links separated by newline. Those links are going to be processed everytime you run *grabber.rb*.
